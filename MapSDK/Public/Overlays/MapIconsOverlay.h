/***********************************************************************
 *
 * aisle411
 * Copyright (C) by aisle411
 * http://www.aisle411.com
 *
 * Developed by Mercury Development, LLC
 * http://www.mercdev.com
 *
 ***********************************************************************/

#import "CalloutOverlay.h"
#import "MapIconOverlayItem.h"

@interface MapIconsOverlay : CalloutOverlay

@end
