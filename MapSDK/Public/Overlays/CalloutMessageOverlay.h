/***********************************************************************
 *
 * aisle411
 * Copyright (C) by aisle411
 * http://www.aisle411.com
 *
 * Developed by Mercury Development, LLC
 * http://www.mercdev.com
 *
 ***********************************************************************/

#import <Foundation/Foundation.h>
#import "CalloutOverlay.h"
#import "CalloutView.h"

/*!
 @class CalloutMessageOverlay
 @abstract CalloutMessageOverlay class definition.
 */
@interface CalloutMessageOverlay : CalloutOverlay<CalloutViewDelegate> {

}

@end
