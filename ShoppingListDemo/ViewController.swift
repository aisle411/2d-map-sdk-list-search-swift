//
//  ViewController.swift
//  ShoppingListDemo
//
//  Created by Hayden Mans on 6/22/17.
//  Copyright © 2017 Hayden Mans. All rights reserved.
//

import UIKit

struct Section {
    let item_location_id: Int
    let map_location_id: Int
    let item_sub_location_id: Int
    let aisle: String
    let section: String
}

struct Product {
    let synonym_id: Int
    let synonym_nm: String
    let name: String
    let sections: [Section]
}

func convertToDictionary(text: String) -> [String: Any]? {
    if let data = text.data(using: .utf8) {
        do {
            return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
        } catch {
            print(error.localizedDescription)
        }
    }
    return nil
}

func convertProducts(text: String) -> [String: AnyObject]? {
    if let data = text.data(using: .utf8) {
        do {
            return try JSONSerialization.jsonObject(with: data, options: []) as? [String: AnyObject]
        } catch {
            print(error.localizedDescription)
        }
    }
    return nil
}

extension Product {
    init?(json: [String:Any]){
        print(json)
        guard let name = json["name"] as? String,
            let synonym_id = json["synonym_id"] as? Int,
            let synonym_nm = json["synonym_nm"] as? String,
            let sectionsJSON = json["sections"] as? [AnyObject]
            else {
                return nil
        }
        var sections: [Section] = []
        for part in sectionsJSON {
            let section = Section(item_location_id: part["item_location_id"] as! Int, map_location_id: part["map_location_id"] as! Int,
                                  item_sub_location_id: part["item_sub_location_id"] as! Int, aisle: part["aisle"] as! String,
                                  section: part["section"] as! String)
            sections.append(section)
        }
        self.synonym_id = synonym_id
        self.synonym_nm = synonym_nm
        self.name = name
        self.sections = sections
    }
}

func MD5(string: String) -> Data {
    let messageData = string.data(using:.utf8)!
    var digestData = Data(count: Int(CC_MD5_DIGEST_LENGTH))
    
    _ = digestData.withUnsafeMutableBytes {digestBytes in
        messageData.withUnsafeBytes {messageBytes in
            CC_MD5(messageBytes, CC_LONG(messageData.count), digestBytes)
        }
    }
    
    return digestData
}

extension String  {
    var isNumber : Bool {
        get{
            return !self.isEmpty && self.rangeOfCharacter(from: CharacterSet.decimalDigits.inverted) == nil
        }
    }
}

extension String
{
    func encodeUrl() -> String
    {
        return self.addingPercentEncoding( withAllowedCharacters: NSCharacterSet.urlQueryAllowed)!
    }
    func decodeUrl() -> String
    {
        return self.removingPercentEncoding!
    }
    
}


class ViewController: UIViewController {

    @IBOutlet weak var mapView: UIView!
    
    var productCallOutOverlay: ProductCalloutOverlay!
    
    var mapController: MapController!
    var mapBundle: MapBundle!
    var mapParser: MapBundleParser!
    var activeMap: Int = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        self.loadMap()
        self.productCallOutOverlay = ProductCalloutOverlay()
        self.productCallOutOverlay.products = []
        self.productCallOutOverlay.showChevronInCallout = true
        self.productCallOutOverlay.delegate = self
        let list = ["bread", "apples", "pickles", "dog food", "potato chips", "milk", "soda"]
        self.shoppingListSearch(storeId: self.activeMap, list: list)
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadMap(){
        self.activeMap = 1141489
        self.getStoreMap(storeId: self.activeMap)
        self.productCallOutOverlay = ProductCalloutOverlay()
        self.productCallOutOverlay.products = []
        self.productCallOutOverlay.showChevronInCallout = true
        self.productCallOutOverlay.delegate = self
    }
    
    func getStoreMap(storeId: Int){
        //1a. Check if map containing folder exists locally
        //1b. If folder doesn't exist, create it
        //2a. Check if map exists locally
        //2b. If it does, load it
        //2c. If it doesn't, download it and save it, then load it
        let documentsUrl:URL = FileManager.default.urls(for: .documentDirectory, in:
            .userDomainMask).first as URL!
        let paths:NSArray = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true) as NSArray
        let documentsDirectory = paths.object(at: 0) as! NSString
        let folderPath = documentsDirectory.appendingPathComponent("StoredMaps")
        let fileManager = FileManager()
        if fileManager.fileExists(atPath: folderPath) {}//if directory exists, continue
        else { //else, try to create the directory
            print("Directory not found.")
            do {
                try FileManager.default.createDirectory(atPath: folderPath, withIntermediateDirectories: false,attributes:  nil)
            }   catch let error as NSError {
                print(error.localizedDescription);
            }
        }
        let destinationFilePath =
            documentsDirectory.appendingPathComponent("StoredMaps/\(storeId).imap")
        let destinationFileUrl =
            documentsUrl.appendingPathComponent("StoredMaps/\(storeId).imap")
        print("\(destinationFilePath)")
        if fileManager.fileExists(atPath: destinationFilePath) {
            print("Found map file.")
            DispatchQueue.global().async {
                self.mapParser = MapBundleParser(pathToArchive: destinationFilePath)!
                self.mapBundle = self.mapParser.parse()
                DispatchQueue.main.async {
                    self.mapController = MapController()
                    self.mapController.mapBundle = self.mapBundle
                    self.mapController.view.frame = self.mapView.bounds //self.mapView.bounds
                    self.mapController.floorLevel = 1
                    self.mapController.setZoomButtonsHidden(true)
                    self.mapController.setCompassEnabled(false)
                    self.mapController.logoPosition = AisleLogoRightBottomPosition
                    self.mapView.addSubview(self.mapController.view)
                    //self.indicator.stopAnimating()
                }
            }
        } //if directory exists, do not download map file
        else {
            let func_nm = "map"
            let part_id = 71
            let secret = "(Cyp<r7MZ=8]=a"
            let base = "https://qa.aisle411.ws/webservices3/\(func_nm).php?"
            let lat = 38.6256740
            let long = -90.1892740
            let store_id = storeId
            var sendurl = ""
            let uappend = "\(func_nm)?latitude=\(lat)&longitude=\(long)&partner_id=\(part_id)&retailer_store_id=\(store_id)&\(secret)"
            let jumbleData = MD5(string:uappend)
            let jumble = jumbleData.map { String(format: "%02hhx", $0) }.joined()
            let auth = "auth=\(jumble)"
            let finappend = "latitude=\(lat)&longitude=\(long)&partner_id=\(part_id)&retailer_store_id=\(store_id)&\(auth)"
            sendurl = base + finappend
            let url = URL(string: sendurl)
            let sessionConfig = URLSessionConfiguration.default
            let session = URLSession(configuration: sessionConfig)
            let request = URLRequest(url:url!)
            let task = session.downloadTask(with: request) { (tempLocalUrl, response, error) in
                if let tempLocalUrl = tempLocalUrl, error == nil {
                    if let statusCode = (response as? HTTPURLResponse)?.statusCode {
                        print("Successfully downloaded. Status code: \(statusCode)")
                    }
                    do {
                        try FileManager.default.copyItem(at: tempLocalUrl, to: destinationFileUrl)
                    } catch {
                        print("Failed to copy file")
                    }
                    if (fileManager.fileExists(atPath: destinationFilePath)) {
                        DispatchQueue.global().async {
                            self.mapParser = MapBundleParser(pathToArchive: destinationFilePath)!
                            self.mapBundle = self.mapParser.parse()
                            DispatchQueue.main.async {
                                self.mapController = MapController()
                                self.mapController.mapBundle = self.mapBundle
                                self.mapController.view.frame = self.mapView.bounds //self.mapView.bounds
                                self.mapController.floorLevel = 1
                                self.mapController.setZoomButtonsHidden(true)
                                self.mapController.setCompassEnabled(false)
                                self.mapController.logoPosition = AisleLogoRightBottomPosition
                                self.mapView.addSubview(self.mapController.view)
                            }
                        }
                    }
                    else {
                        print("Error: Failed to load map downloaded from webservices.")
                    }
                }
            }
            task.resume()
        }
    }

    func shoppingListSearch(storeId: Int, list: [String]){
        let devtok = UIDevice.current.identifierForVendor!.uuidString
        let func_nm = "locateitems"
        let part_id = 71
        let secret = "(Cyp<r7MZ=8]=a"
        let base = "https://qa.aisle411.ws/webservices3/\(func_nm).php?";
        var body : [String:Any] = [:]
        let shoplist = buildShoppingList(list: list)
        body["device_token"] = devtok;
        body["partner_id"] = part_id;
        body["retailer_store_id"] = storeId;
        body["shopping_list_data"] = shoplist;
        body["latitude"] = 38.625674;
        body["longitude"] = -90.1892740;
        var sendbody : Data
        do {
            sendbody = try JSONSerialization.data(withJSONObject: body, options: .prettyPrinted)
        } catch {
            return
        }
        let sendstring = String(data: sendbody, encoding: String.Encoding.utf8) as String!
        let appendage = sendstring! + secret;
        let jumbleData = MD5(string: appendage)
        let jumble = jumbleData.map { String(format: "%02hhx", $0) }.joined()
        var request = URLRequest(url: URL(string: base)!)
        request.httpMethod = "POST"
        request.addValue(jumble, forHTTPHeaderField: "Authentication")
        request.httpBody = sendbody
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard let data = data, error == nil else {
                print("error=\(String(describing: error))")
                return
            }
            
            if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 {
                print("statusCode should be 200, but is \(httpStatus.statusCode)")
                print("response = \(String(describing: response))")
            }
            
            let responseString = String(data: data, encoding: .utf8)
            print("responseString = \(String(describing: responseString))")
            if responseString != nil {
                self.parseResults(results: responseString!)
            }
        }
        task.resume()
    }
    
    func buildShoppingList(list: [String]) -> [String:Any]{
        var shoplist: [String:Any] = [:]
        shoplist["name"] = "Default Shopping List"
        var items = [Any]()
        var count = 0
        for item in list {
            if item.isNumber {
                items.append(["upc": item])
            }
            else {
                items.append(["name": item.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed)!])
            }
            count += 1
        }
        shoplist["items"] = items
        return shoplist
    }
    
    func parseResults(results: String){
        let parsed = convertProducts(text: results)!
        var products: [FMProduct] = []
        let items = parsed["items"]! as! [Any]
        for item in items {
            let product = Product(json: item as! [String: Any])
            for section in product!.sections {
                let temp = FMProduct()
                let tempsection = FMSection(sublocation: Int32(section.item_sub_location_id), location: Int32(section.item_location_id))
                temp.name = product!.name.decodeUrl()
                temp.idn = Int32(product!.synonym_id)
                let sectionlist : [String] = []
                temp.sections = sectionlist
                tempsection!.maplocation = Int32(section.map_location_id)
                tempsection!.aisleTitle = section.aisle
                tempsection!.title = section.section
                temp.sections.append(tempsection!)
                products.append(temp)
            }
        }
        print(products)
        self.productCallOutOverlay.products = products
        DispatchQueue.main.async {
            self.mapController.remove(self.productCallOutOverlay)
            self.mapController.add(self.productCallOutOverlay)
        }
    }

}

extension ViewController: CalloutOverlayDelegate {
    func calloutOverlay(_ overlay: CalloutOverlay!, didItemSelected item: OverlayItem!) {
        
    }
    
    func calloutOverlay(_ overlay: CalloutOverlay!, didItemDeselected item: OverlayItem!) {
        
    }
    
    func calloutOverlay(_ overlay: CalloutOverlay!, didItemReselected oldItem: OverlayItem!, with item: OverlayItem!) {

    }
}

